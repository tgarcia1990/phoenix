defmodule Project.Repo.Migrations.CreateEmployees do
  use Ecto.Migration

  def change do
    create table(:employees) do
      add :name, :string
      add :age, :integer, default: 0, null: false
      add :salary, :decimal
      add :profetion, :string
      add :gender, :string
      add :company_id, references(:companys, on_delete: :delete_all)

      timestamps()
    end
  end
end
