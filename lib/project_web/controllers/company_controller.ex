defmodule ProjectWeb.CompanyController do
  use ProjectWeb, :controller

  alias Project.Organization
  alias Project.Organization.Company

  def index(conn, _params) do
    companys = Organization.list_companys()
    render(conn, "index.html", companys: companys)
  end

  def menu(conn, %{"id" => id}=_params) do

    render(conn, "menu.html",id: String.to_integer(id))
  end

  def new(conn, _params) do
    changeset = Organization.change_company(%Company{})
    render(conn, "new.html", changeset: changeset)
  end

  def create(conn, %{"company" => company_params}) do
    case Organization.create_company(company_params) do
      {:ok, company} ->
        conn
        |> put_flash(:info, "Company created successfully.")
        |> redirect(to: Routes.company_path(conn, :show, company))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "new.html", changeset: changeset)
    end
  end

  def show(conn, %{"id" => id}) do
    company = Organization.get_company!(id)
    render(conn, "show.html", company: company)
  end

  def edit(conn, %{"id" => id}) do
    company = Organization.get_company!(id)
    changeset = Organization.change_company(company)
    render(conn, "edit.html", company: company, changeset: changeset)
  end

  def update(conn, %{"id" => id, "company" => company_params}) do
    company = Organization.get_company!(id)

    case Organization.update_company(company, company_params) do
      {:ok, company} ->
        conn
        |> put_flash(:info, "Company updated successfully.")
        |> redirect(to: Routes.company_path(conn, :show, company))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "edit.html", company: company, changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id}) do
    company = Organization.get_company!(id)
    {:ok, _company} = Organization.delete_company(company)
    conn
    |> put_flash(:info, "Company deleted successfully.")
    |> redirect(to: Routes.company_path(conn, :index))
  end
end
