defmodule Project.Organization.Company do
  use Ecto.Schema
  import Ecto.Changeset

  schema "companys" do
    field :description, :string
    field :name, :string
    has_many :employees, Project.Staff.Employee


    timestamps()
  end

  @doc false
  def changeset(company, attrs) do
    company
    |> cast(attrs, [:name, :description])
    |> validate_required([:name, :description])
  end
end
