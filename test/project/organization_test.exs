defmodule Project.OrganizationTest do
  use Project.DataCase

  alias Project.Organization

  describe "companys" do
    alias Project.Organization.Company

    import Project.OrganizationFixtures

    @invalid_attrs %{description: nil, name: nil}

    test "list_companys/0 returns all companys" do
      company = company_fixture()
      assert Organization.list_companys() == [company]
    end

    test "get_company!/1 returns the company with given id" do
      company = company_fixture()
      assert Organization.get_company!(company.id) == company
    end

    test "create_company/1 with valid data creates a company" do
      valid_attrs = %{description: "some description", name: "some name"}

      assert {:ok, %Company{} = company} = Organization.create_company(valid_attrs)
      assert company.description == "some description"
      assert company.name == "some name"
    end

    test "create_company/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Organization.create_company(@invalid_attrs)
    end

    test "update_company/2 with valid data updates the company" do
      company = company_fixture()
      update_attrs = %{description: "some updated description", name: "some updated name"}

      assert {:ok, %Company{} = company} = Organization.update_company(company, update_attrs)
      assert company.description == "some updated description"
      assert company.name == "some updated name"
    end

    test "update_company/2 with invalid data returns error changeset" do
      company = company_fixture()
      assert {:error, %Ecto.Changeset{}} = Organization.update_company(company, @invalid_attrs)
      assert company == Organization.get_company!(company.id)
    end

    test "delete_company/1 deletes the company" do
      company = company_fixture()
      assert {:ok, %Company{}} = Organization.delete_company(company)
      assert_raise Ecto.NoResultsError, fn -> Organization.get_company!(company.id) end
    end

    test "change_company/1 returns a company changeset" do
      company = company_fixture()
      assert %Ecto.Changeset{} = Organization.change_company(company)
    end
  end
end
