defmodule ProjectWeb.EmployeeController do
  use ProjectWeb, :controller

  alias Project.Staff
  alias Project.Staff.Employee


  def index(conn, %{"id" => id,"company"=> company}=_params) do

    resp=if String.to_integer(company)==0 do
      1
    else
      0
    end

    if String.to_integer(id)==4 and String.to_integer(company)==0  do
      getgender(conn,company,resp)
    end
    if String.to_integer(id)==4 do
      getgender_by_org(conn,company,resp)
    end

    employees= if String.to_integer(company)==0 do
       Staff.list_all_employees()
    else
       Staff.list_all_employees_by_org(String.to_integer(company))
    end
    sum = gettotal(employees)
    render(conn, "all_employee.html", employees: employees,id: String.to_integer(id),company: String.to_integer(company),total: Enum.sum(sum),resp: resp)
  end



  def getgender(conn,company,resp) do
    femenino = Staff.list_all_gender("Femenino")
    x = Enum.map(femenino, fn x -> x  end)
    masculino = Staff.list_all_gender("Masculino")
    y = Enum.map(masculino, fn y -> y  end)

    xy=%{:femenino => Enum.sum(x), :masculino => Enum.sum(y)}

    render(conn, "all_employee_name.html", employees: xy,company: String.to_integer(company),resp: resp)

  end

  def getgender_by_org(conn,company,resp) do
    femenino = Staff.list_all_gender_org("Femenino",company)
    x = Enum.map(femenino, fn x -> x  end)
    masculino = Staff.list_all_gender_org("Masculino",company)
    y = Enum.map(masculino, fn y -> y  end)
    xy=%{:femenino => Enum.sum(x), :masculino => Enum.sum(y)}

    render(conn, "all_employee_name.html", employees: xy,company: String.to_integer(company),resp: resp)
  end

  def gettotal(employees) do
    sum=[]
    sum = for employee <- employees do
      sum++ employee.employee.salary|>Decimal.round()|> Decimal.to_integer()
    end
    sum

  end

  def show_all(conn, %{"id" => id}) do
    employees = Staff.list_employees_by_org(id)
    render(conn, "index.html", employees: employees, id: id)
  end


  @spec new(Plug.Conn.t(), nil | maybe_improper_list | map) :: Plug.Conn.t()
  def new(conn,%{"id" => id}=_params) do
    changeset = Staff.change_employee(%Employee{:company_id=>String.to_integer(id)})
    render(conn, "new.html", changeset: changeset,id: String.to_integer(id))
  end

  def create(conn, %{"employee" => employee_params}) do
    %{"company_id" => company_id}=employee_params
    case Staff.create_employee(employee_params) do
      {:ok, employee} ->
        conn
        |> put_flash(:info, "Employee created successfully.")
        |> redirect(to: Routes.employee_path(conn, :show, employee))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "new.html", changeset: changeset,id: String.to_integer(company_id))
    end
  end

  def show(conn, %{"id" => id}) do
    employee = Staff.get_employee!(id)
    render(conn, "show.html", employee: employee)
  end

  def edit(conn, %{"id" => id}) do
    employee = Staff.get_employee!(id)
    changeset = Staff.change_employee(employee)
    render(conn, "edit.html", employee: employee, changeset: changeset)
  end

  def update(conn, %{"id" => id, "employee" => employee_params}) do
    employee = Staff.get_employee!(id)

    case Staff.update_employee(employee, employee_params) do
      {:ok, employee} ->
        conn
        |> put_flash(:info, "Employee updated successfully.")
        |> redirect(to: Routes.employee_path(conn, :show, employee))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "edit.html", employee: employee, changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id}) do
    employee = Staff.get_employee!(id)
    {:ok, _employee} = Staff.delete_employee(employee)
    conn
    |> put_flash(:info, "Employee deleted successfully.")
    |> render("show.html", employee: employee)
  end
end
