defmodule Project.Repo.Migrations.CreateCompanys do
  use Ecto.Migration

  def change do
    create table(:companys) do
      add :name, :string
      add :description, :string

      timestamps()
    end
  end
end
