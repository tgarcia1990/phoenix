defmodule Project.Staff.Employee do
  use Ecto.Schema
  import Ecto.Changeset

  schema "employees" do
    field :age, :integer
    field :gender, :string
    field :name, :string
    field :profetion, :string
    field :salary, :decimal
    belongs_to :company, Project.Organization.Company

    timestamps()
  end

  @doc false
  def changeset(employee, attrs) do
    employee
    |> cast(attrs, [:name, :age, :salary, :profetion, :gender,:company_id])
    |> validate_required([:name, :age, :salary, :profetion, :gender,:company_id])
  end
end
