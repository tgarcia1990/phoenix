defmodule Project.Staff do
  @moduledoc """
  The Staff context.
  """

  import Ecto.Query, warn: false
  alias Project.Repo

  alias Project.Staff.Employee
  alias Project.Organization.Company

  @doc """
  Returns the list of employees.

  ## Examples

      iex> list_employees()
      [%Employee{}, ...]

  """
  def list_employees do
    Repo.all(Employee)
  end

  def list_all_employees do
    Repo.all(from e in Employee, join: c in Company, on: e.company_id==c.id, select: %{employee: e,org: c.name} )

  end

  def list_all_employees_by_org(id) do
    Repo.all(from e in Employee, join: c in Company, on: e.company_id==c.id, where: e.company_id == ^id, select: %{employee: e,org: c.name} )

  end

  def list_all_gender(id) do
    Repo.all(from e in Employee, where: e.gender == ^id, select: count(e.gender))

  end

  def list_all_gender_org(id, company) do
    Repo.all(from e in Employee, where: e.gender == ^id and e.company_id == ^company, select: count(e.gender))

  end



  def list_employees_by_org(nil), do: []

  def list_employees_by_org(id) do

    Repo.all(from e in Employee, where: e.company_id == ^id)

  end

  @doc """
  Gets a single employee.

  Raises `Ecto.NoResultsError` if the Employee does not exist.

  ## Examples

      iex> get_employee!(123)
      %Employee{}

      iex> get_employee!(456)
      ** (Ecto.NoResultsError)

  """
  def get_employee!(id), do: Repo.get!(Employee, id)

  @doc """
  Creates a employee.

  ## Examples

      iex> create_employee(%{field: value})
      {:ok, %Employee{}}

      iex> create_employee(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_employee(attrs \\ %{}) do
    %Employee{}
    |> Employee.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a employee.

  ## Examples

      iex> update_employee(employee, %{field: new_value})
      {:ok, %Employee{}}

      iex> update_employee(employee, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_employee(%Employee{} = employee, attrs) do
    employee
    |> Employee.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a employee.

  ## Examples

      iex> delete_employee(employee)
      {:ok, %Employee{}}

      iex> delete_employee(employee)
      {:error, %Ecto.Changeset{}}

  """
  def delete_employee(%Employee{} = employee) do
    Repo.delete(employee)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking employee changes.

  ## Examples

      iex> change_employee(employee)
      %Ecto.Changeset{data: %Employee{}}

  """
  def change_employee(%Employee{} = employee, attrs \\ %{}) do
    Employee.changeset(employee, attrs)
  end
end
