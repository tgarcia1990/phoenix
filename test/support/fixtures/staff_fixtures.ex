defmodule Project.StaffFixtures do
  @moduledoc """
  This module defines test helpers for creating
  entities via the `Project.Staff` context.
  """

  @doc """
  Generate a employee.
  """
  def employee_fixture(attrs \\ %{}) do
    {:ok, employee} =
      attrs
      |> Enum.into(%{
        age: 42,
        gender: "some gender",
        name: "some name",
        profetion: "some profetion",
        salary: "120.5"
      })
      |> Project.Staff.create_employee()

    employee
  end
end
