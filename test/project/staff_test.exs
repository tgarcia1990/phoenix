defmodule Project.StaffTest do
  use Project.DataCase

  alias Project.Staff

  describe "employees" do
    alias Project.Staff.Employee

    import Project.StaffFixtures

    @invalid_attrs %{age: nil, gender: nil, name: nil, profetion: nil, salary: nil}

    test "list_employees/0 returns all employees" do
      employee = employee_fixture()
      assert Staff.list_employees() == [employee]
    end

    test "get_employee!/1 returns the employee with given id" do
      employee = employee_fixture()
      assert Staff.get_employee!(employee.id) == employee
    end

    test "create_employee/1 with valid data creates a employee" do
      valid_attrs = %{age: 42, gender: "some gender", name: "some name", profetion: "some profetion", salary: "120.5"}

      assert {:ok, %Employee{} = employee} = Staff.create_employee(valid_attrs)
      assert employee.age == 42
      assert employee.gender == "some gender"
      assert employee.name == "some name"
      assert employee.profetion == "some profetion"
      assert employee.salary == Decimal.new("120.5")
    end

    test "create_employee/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Staff.create_employee(@invalid_attrs)
    end

    test "update_employee/2 with valid data updates the employee" do
      employee = employee_fixture()
      update_attrs = %{age: 43, gender: "some updated gender", name: "some updated name", profetion: "some updated profetion", salary: "456.7"}

      assert {:ok, %Employee{} = employee} = Staff.update_employee(employee, update_attrs)
      assert employee.age == 43
      assert employee.gender == "some updated gender"
      assert employee.name == "some updated name"
      assert employee.profetion == "some updated profetion"
      assert employee.salary == Decimal.new("456.7")
    end

    test "update_employee/2 with invalid data returns error changeset" do
      employee = employee_fixture()
      assert {:error, %Ecto.Changeset{}} = Staff.update_employee(employee, @invalid_attrs)
      assert employee == Staff.get_employee!(employee.id)
    end

    test "delete_employee/1 deletes the employee" do
      employee = employee_fixture()
      assert {:ok, %Employee{}} = Staff.delete_employee(employee)
      assert_raise Ecto.NoResultsError, fn -> Staff.get_employee!(employee.id) end
    end

    test "change_employee/1 returns a employee changeset" do
      employee = employee_fixture()
      assert %Ecto.Changeset{} = Staff.change_employee(employee)
    end
  end
end
